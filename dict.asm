section .text

global find_word

extern string_equals

;rdi - key
;rsi - pointer to the begining of the dictionary

find_word:
.loop:
    push rsi
    push rdi
    add rsi, 8 ;skip the ending symbol of the key
    call string_equals
    pop rdi
    pop rsi
    cmp rax, 0
    jne .success 
    mov rsi, [rsi]
    cmp rsi, 0
    je .fail ;check if the dictionary ended
    jmp .loop
.success:
	mov rax, rsi
	ret
.fail:
	xor rax, rax
    ret