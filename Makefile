ASM=nasm
ASMFLAGS=-f elf64
LD=ld

lab2: main.o lib.o dict.o
	$(LD) -o $@ $^

main.o: main.asm lib.inc words.inc colon.inc

dict.o: dict.asm lib.inc

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<
