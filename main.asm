%include "colon.inc"

section .bss
buffer: resb 256 ; reserve 256 bytes

section .rodata
%include "words.inc"
start_msg: db 'Enter the key, please: ', 0
no_key_err: db 'There is no such key in the dictionary', 0
long_key_err: db 'Key is too long', 0

section .data

section .text
%include "lib.inc"


global _start
extern find_word

_start:
    mov rdi, start_msg
    call print_string
    
    xor rcx, rcx 

    .loop:
        push rcx
		call read_char
		pop rcx
		cmp rax, 0
		je .find_str
		cmp al, 0xA
		je .find_str
		
		cmp rcx, 255
		je .long_key_error
		
		mov byte[buffer + rcx], al
		inc rcx
		
		jmp .loop

    .find_str:
        mov rdi, buffer
        mov rsi, dict_start
        call find_word
        cmp rax, 0
        je .no_key_error
    
    mov rdi, rax
    add rdi, 8
    push rdi
	call string_length
	pop rdi	
	add rdi, rax
	inc rdi

   	
   	call print_string
   	call print_newline
   	call exit
       
    .no_key_error:
        mov rdi, no_key_err
        call print_error
        call exit
    .long_key_error:
        mov rdi, long_key_err
        call print_error
        call exit
