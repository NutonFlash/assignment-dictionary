section .text

global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_error
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
	mov rax,60
    	syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
;rax - length
;rdi - first symbol
string_length:
	xor rax, rax
	.loop:                    ;compare the symbol with zero
		cmp byte [rdi+rax], 0 ;to detect the end of the string
		je .end               
		inc rax ;increase the length 
		jmp .loop
	.end:
		ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	call string_length ;write the length in rax
	mov rdx, rax ;copy rax to rdx
	mov rsi, rdi ;copy address of the begining of the line to rsi 
	mov rax, 1 ;sys_write
	mov rdi, 1 ;stdout
	syscall 
	ret

; Принимает код символа и выводит его в stdout
print_char:
	push rdi ;push the symbol to the stack
	mov rsi,rsp ;copy address of the top of the stack to rsi
	mov rdx,1 ;lenght - 1 byte
	mov rax,1 ;sys_write
	mov rdi,1 ;stdout
	syscall
	pop rdi ;take off the symbol from the stack
   	ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov rdi,0xA ;code of the new_line symbol
    jmp print_char ;executing print_char function 

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	xor rax, rax
	times 11 push word 0 ;20 bytes + "0"
	mov rax,rdi ;rax - number
	mov r8,10 ;scale of notation
	mov r9, rsp ;copy tops address of the stack
	add rsp, 21 ;move rsp to the begining of the number
	.loop:
		xor rdx, rdx 
		dec rsp ;move link
		div r8 ;divide on ten 
		add rdx, "0" ;convert to ascii
		mov byte [rsp],dl ;move symbol to the stack
		test rax,rax 
		jnz .loop 
	mov rdi, rsp ;address of the number in stack
	call print_string
	mov rsp, r9
	add rsp, 22 ;back to the previous link
	ret
    

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
	test rdi, rdi
	jns print_uint ;if number > 0 then print uint
	push rdi
	mov rdi, "-" ;else print minus
	call print_char
	pop rdi
	neg rdi ; convert to positive number
	jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    mov al, byte [rdi]
    cmp al, byte [rsi]
    jne .no
    inc rdi
    inc rsi
    test al, al
    jnz string_equals
    mov rax, 1
    ret
.no:
    xor rax, rax
    ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rsi, rsp
    mov rdx, 1 ;symbol length - 1 byte
    xor rax, rax ;sys_read
    xor rdi, rdi ;stdin
    syscall
    pop rax
    ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push r13 
    push r12
    xor r12, r12 
    xor r13, r13
    mov r13, rdi
	.skip_space:
		call read_char 
		cmp rax, 0 ;compare with the ending symbol
		je .success      
		cmp rax, 0x20 ;check if the symbol is space
		je .skip_space
		cmp rax, 0x9 ;check if the symbol is horizontal tab
		je .skip_space
		cmp rax, 0xA ;check if the symbol is line feed
		je .skip_space
	.read_symbol:
		cmp rsi, r12
		je .overbuf
		mov byte[r13 + r12], al
		inc r12
		call read_char
		cmp rax, 0
		je .success
		cmp rax, 0x9
		je .success
		cmp rax, 0x20
		je .success
		cmp rax, 0xA
		je .success
		jmp .read_symbol
	.success:
		xor rax, rax
		mov byte[r13 + r12], al
		mov rax, r13
		mov rdx, r12
		jmp .end
	.overbuf:
		xor rax, rax
	.end:
		pop r12 
		pop r13
		ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
	xor rdx, rdx
	xor rcx, rcx ;counter
	xor r9, r9
	mov r9, 10 ;scale of notation
	xor r10, r10
	mov r10, "9"; max number
	xor r11, r11 ; buffer

	
	.loop:
		mov r11b, byte[rdi, rcx]
		cmp r11b, "0" ;check if the symbol is end of the number
		js .break
		cmp r10b, r11b ;check if the simbol is bigger than 9
		js .break
		sub r11, "0"
		mul r9 ;increase number rank 
		add rax, r11
		inc rcx ;increase symbols counter
		test rax, rax ;check if the number equals zero
		jnz .loop
	.break:
		mov rdx, rcx ;write symbols counter in rdx
		ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
	cmp byte[rdi], "-" ;check if the number starts with minus symbol
	je .inverse
	jmp parse_uint;if the number is positive
	.inverse: 
		inc rdi;skip the symbol of minus
		call parse_uint
		inc rdx;increment the length of the string
		neg rax;inverse the number
		ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
	xor rcx, rcx
	push r11
	.copy:
		cmp rcx, rdx 
		je .end
		mov r11, [rdi+rcx]
		mov [rsi+rcx], r11
		cmp r11, 0
		je .exit
		inc rcx
		jmp .copy
	.end:
		xor rax, rax
	.exit:
		pop r11
		ret

print_error:
	call string_length
    mov rdx, rax
    mov rsi, rdi
    mov rdi, 2 ;stderr
    mov rax, 1
    syscall
	ret		
